# Windows client TP1

## Host OS 

### Déterminer les principales informations de votre machine 

* Nom de la machine, OS et version :
```
PS C:\Users\rukab> systeminfo
Nom de l’hôte:                               DESKTOP-HO10B5L
Nom du système d’exploitation:               Microsoft Windows 10 Famille
Version du système:                          10.0.18363 N/A version 18363
[...]
```
* Architecture processeur
```
C:\Users\rukab>set
[...]
PROCESSOR_ARCHITECTURE=AMD64
[...]
```
* Quantité de RAM
```
PS C:\Users\rukab> systeminfo
[...]
Mémoire physique totale:                       16,165 Mo
[...]
```
* Modèle de la RAM
```
PS C:\Users\rukab> wmic MemoryChip
        PartNumber 
[...]   8ATF1G64HZ-2G6E1  1           
        8ATF1G64HZ-2G6E1  1 [...]   
```
## Devices : 

### Trouver 

* La Marque et le Modèle du processeur
```
C:\Users\rukab>wmic cpu get caption, deviceid, name, numberofcores, maxclockspeed, status

Caption                               DeviceID  MaxClockSpeed 
Intel64 Family 6 Model 158 Stepping 10  CPU0      2592       
Name                                      NumberOfCores  Status
Intel(R) Core(TM) i7-9750H CPU @ 2.60GHz  6                OK
```
1. le(s) Nombre de processeur(s) et le(s) nombre de coeur(s)
```
C:\Users\rukab>wmic cpu get NumberOfCores, NumberOfLogicalProcessors/Format:List

NumberOfCores=6
NumberOfLogicalProcessors=12
```
2. Si c'est un proc Intel, expliquer le nom du processeur

Mon processeur est un i7 de 9e génération, les chiffres 900 sont le numero du processeur et le H signifie qu'il appartient à la série H, une série de processeurs mobiles puissants pour PC portables.


* La Marque et le Modèle
1. Du touchpad/trackpad
```
PS C:\Users\rukab> gwmi Win32_PointingDevice                                                                    


__GENUS                     : 2
__CLASS                     : Win32_PointingDevice
__SUPERCLASS                : CIM_PointingDevice
__DYNASTY                   : CIM_ManagedSystemElement
__RELPATH                   : Win32_PointingDevice.DeviceID="HID\\DELL0923&COL01\\5&1CCBF562&0&0000"
__PROPERTY_COUNT            : 33
__DERIVATION                : {CIM_PointingDevice, CIM_UserDevice, CIM_LogicalDevice, CIM_LogicalElement...}
__SERVER                    : DESKTOP-HO10B5L
__NAMESPACE                 : root\cimv2
__PATH                      : \\DESKTOP-HO10B5L\root\cimv2:Win32_PointingDevice.DeviceID="HID\\DELL0923&COL01\\5&1CCBF562&0&0000"
Availability                :
Caption                     : Souris HID
ConfigManagerErrorCode      : 0
ConfigManagerUserConfig     : False
CreationClassName           : Win32_PointingDevice
Description                 : Souris HID
DeviceID                    : HID\DELL0923&COL01\5&1CCBF562&0&0000
DeviceInterface             : 162
DoubleSpeedThreshold        :
ErrorCleared                :
ErrorDescription            :
Handedness                  :
HardwareType                : Souris HID
InfFileName                 : msmouse.inf
InfSection                  : HID_Mouse_Inst.NT
InstallDate                 :
IsLocked                    :
LastErrorCode               :
Manufacturer                : Microsoft
Name                        : Souris HID
NumberOfButtons             : 0
PNPDeviceID                 : HID\DELL0923&COL01\5&1CCBF562&0&0000
PointingType                : 2
PowerManagementCapabilities :
PowerManagementSupported    : False
QuadSpeedThreshold          :
Resolution                  :
SampleRate                  :
Status                      : OK
StatusInfo                  :
Synch                       :
SystemCreationClassName     : Win32_ComputerSystem
SystemName                  : DESKTOP-HO10B5L
PSComputerName              : DESKTOP-HO10B5L
```
2. De la carte graphique
```
C:\Users\rukab>wmic path win32_VideoController get name

Name
Intel(R) UHD Graphics 630
NVIDIA GeForce GTX 1650
```
### Disque dur

* identifier la marque et le modèle de votre(vos) disque(s) dur(s)
```
C:\Users\rukab>diskpart
DISKPART> list disk

 N° disque  Statut         Taille   Libre    Dyn  GPT
  ---------  -------------  -------  -------  ---  ---
  Disque 0    En ligne        953 G octets    16 M octets        *

DISKPART> select disk 0
Le disque 0 est maintenant le disque sélectionné.

DISKPART> detail disk
Micron 2200S NVMe 1024GB
ID du disque : {70A90F79-08EE-4720-984F-5EF3A5B40363}
{...}
```
* identifier les différentes partitions de votre/vos disque(s) dur(s)
```
DISKPART> select disk 0
DISKPART> list partition

    N° partition   Type              Taille   Décalage
  -------------  ----------------  -------  --------
  Partition 1    Système            150 M   1024 K
  Partition 2    Réservé            128 M    151 M
  Partition 3    Principale         935 G    279 M
  Partition 4    Récupération       990 M    935 G
  Partition 5    Récupération        15 G    936 G
  Partition 6    Récupération      1565 M    952 G
```
* déterminer le système de fichier de chaque partition
```
DISKPART> select partition 1

La partition 1 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 1
Type    : c12a7328-f81f-11d2-ba4b-00a0c93ec93b
Masqué  : Oui
Requis  : Non
Attrib  : 0X8000000000000000
Décalage en octets : 1048576

//Le systeme de fichier de chaque partition ce trouve dans la colone
Fs (ex : partition 1 Fs = FAT32)

    N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 1         ESP          FAT32  Partition    150 M   Sain       Système

DISKPART> select partition 2

La partition 2 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 2
Type    : e3c9e316-0b5c-4db8-817d-f92df00215ae
Masqué  : Oui
Requis  : Non
Attrib  : 0X8000000000000000
Décalage en octets : 158334976

Il n’y a pas de volume associé avec cette partition.

DISKPART> select partition 3

La partition 3 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 3
Type    : ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
Masqué  : Non
Requis  : Non
Attrib  : 0000000000000000
Décalage en octets : 292552704

//Le systeme de fichier de chaque partition ce trouve dans la colone
Fs 
    N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 0     C   OS           NTFS   Partition    935 G   Sain       Démarrag

DISKPART> select partition 4

La partition 4 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 4
Type    : de94bba4-06d1-4d40-a16a-bfd50179d6ac
Masqué  : Non
Requis  : Oui
Attrib  : 0X8000000000000001
Décalage en octets : 1004549439488

//Le systeme de fichier de chaque partition ce trouve dans la colone
Fs 
   N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 2         WINRETOOLS   NTFS   Partition    990 M   Sain       Masqué

DISKPART> select partition 5

La partition 5 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 5
Type    : de94bba4-06d1-4d40-a16a-bfd50179d6ac
Masqué  : Non
Requis  : Oui
Attrib  : 0X8000000000000001
Décalage en octets : 1005587529728

//Le systeme de fichier de chaque partition ce trouve dans la colone
Fs 
  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 2         Image        NTFS   Partition     15 G   Sain       Masqué

DISKPART> select partition 6

La partition 6 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 6
Type    : de94bba4-06d1-4d40-a16a-bfd50179d6ac
Masqué  : Non
Requis  : Oui
Attrib  : 0X8000000000000001
Décalage en octets : 1022552440832

//Le systeme de fichier de chaque partition ce trouve dans la colone
Fs 
 N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 3         DELLSUPPORT  NTFS   Partition   1565 M   Sain       Masqué

  
```
* Expliquer la fonction de chaque partition
```
PS C:\Users\rukab>  Get-Partition

   DiskPath : \\?\scsi#disk&ven_nvme&prod_micron_2200s_nvm#4&2544b336&0&010000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                                                            Size Type
---------------  ----------- ------                                                                            ---- ----
1                            1048576                                                                         150 MB System
2                            158334976                                                                       128 MB Reserved
3                C           292552704                                                                    935.29 GB Basic
4                            1004549439488                                                                   990 MB Recovery
5                            1005587529728                                                                  15.8 GB Recovery
6                            1022552440832                                                                  1.53 GB Recovery
```
**fonction des partition** : 

-System est là ou est l’os du pc
-Reserved est une partition que  Windows utilise comme une partition de démarrage pour charger les informations nécessaires au démarrage de l’ordinateur
-Basic est le lieu de stockage des données utilisateur
-Recovery est une partition dédiée aux options avancées de récupération de Windows

## Users

### Déterminer la liste des utilisateurs de la machine

* la liste complète des utilisateurs de la machine
```
PS C:\Users\rukab> net user

comptes d’utilisateurs de \\DESKTOP-HO10B5L

-------------------------------------------------------------------------------
Administrateur           DefaultAccount           Invité
rukab                    WDAGUtilityAccount
La commande s’est terminée correctement.
```
* déterminer le nom de l'utilisateur qui est full admin sur la machine
```
PS C:\Windows\system32> $objSID = New-Object System.Security.Principal.SecurityIdentifier("S-1-5-20")
>> $objAccount = $objSID.Translate([System.Security.Principal.NTAccount])
>> $objAccount.Value

NT AUTHORITY\NETWORK SERVICE
```
## Processus

###  Déterminer la liste des processus de la machine
```
PS C:\Users\rukab> get-process

Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
-------  ------    -----      -----     ------     --  -- -----------
    408      24    19196      22880       3.39  14868   1 ApplicationFrameHost
    507      29   121720      20104   2,587.41  14424   0 audiodg
   4694     925   301368     101456              4792   0 avp
   1193     126   118124       3088      34.14   8116   1 avpui
     71       5     2332       3968       0.00   1712   1 cmd
    320      20    57788      37092       1.75   3124   1 Code
    644      28    79688      17876       3.02  10008   1 Code
    727      36    33104      40288       3.66  11300   1 Code
    460      33   134868      47132      13.13  18712   1 Code
    252      15     9680      10676       0.05  19792   1 Code
    438      19    13040      22344       0.42  20472   1 Code
    399      24    37236      36208       1.48  20620   1 Code
    163       8     2020       3388       0.98   5732   1 CompPkgSrv
    118       7     6396       1056              3340   0 conhost
    267      14     8452      14828      23.11   8776   1 conhost
    265      13     7620      19152       0.28  20376   1 conhost
    874      27     1988       2280               840   0 csrss
    771      23     2656       2600               964   1 csrss
    489      17     5936       9400      21.92   5276   1 ctfmon
    361      18     5652       7100              2984   0 dasHost
    156       9     1924       1872             16028   0 DDVCollectorSvcApi
    362      24    27596      14320              3376   0 DDVDataCollector
    266      18    17660       4900             12928   0 DDVRulesProcessor
    879     113   126732      45320              9384   0 Dell.D3.WinSvc
    853      49    55176      35420              3032   0 DellSupportAssistRemedationService
    924      32   125284      72672     158.70  11276   1 Discord
    469      24    16456      22628      30.05  11568   1 Discord
    882      47    38908      41388     260.02  12204   1 Discord
    249      18     9044       7712       0.25  14620   1 Discord
    899     120   309676     168284   4,393.70  14660   1 Discord
    329      22    10616      12632       1.50  14780   1 Discord
    236      17     4184       7876       0.38   8392   1 dllhost
    197      17     3528       4124             11484   0 dllhost
    249      13     4044       4824             17352   0 dllhost
    807      63   112060     108812             16288   0 DSAPI
   1345      52   111372      53592              1436   1 dwm
    107       7     1624       1296               816   0 esif_uf
   2955     126    93276     107476     493.28   5648   1 explorer
    238      19    20592      17424   1,973.70   5568   1 firefox
   1412     194   602356     426784   4,206.80   8256   1 firefox
    874      74    98392      62828     156.94  12712   1 firefox
   2559     282   425392     402644   5,977.06  12944   1 firefox
    944     136   465356     173728   1,668.72  13156   1 firefox
   1858     372  1672980     323968  11,744.39  13200   1 firefox
    919     127   432636     126888     936.92  13320   1 firefox
    729     130   600600     110352     276.88  13540   1 firefox
    937     158   679116     159820   2,693.95  13736   1 firefox
    926     129   407772     208156     857.73  15624   1 firefox
    950     158   703772     329320   1,868.41  17060   1 firefox
    898     136   480264     154448   2,195.81  17576   1 firefox
    909     121   374256     118476     821.45  17884   1 firefox
     31       6     1896       1516              1048   0 fontdrvhost
     31      11     6324       4176              1340   1 fontdrvhost
    181      11     1728        152              9664   0 GoogleCrashHandler
    160       9     1724        152              9708   0 GoogleCrashHandler64
      0       0       60          8                 0   0 Idle
    383      33    17376      12676       0.64  12320   1 IGCC
    657      36    48580      34160       4.67  11892   1 IGCCTray
    179      10     2084       2076              2688   0 igfxCUIService
   1192      26     8500      13748       8.63   5904   1 igfxEM
    376      23    28488      13744              5672   0 IntelAudioService
    165       8     1760       3688              6120   0 IntelCpHDCPSvc
    137       7     1636       3036              6812   0 IntelCpHeciSvc
    125       7     1132        812              6660   0 jhi_service
    159      10     1476       2396       0.05  12964   1 jusched
    797     618    29660      12560              9636   0 ksde
    312      19     7428       2664       1.34  17616   1 ksdeui
    261      13     4372       3896              5944   0 LMS
    553      26    16972      31184       5.58  16652   1 LockApp
   1828      32     9520      17200               108   0 lsass
      0       0     4640     223380              3192   0 Memory Compression
    976      52    53476       1140       1.19  15756   1 Microsoft.Photos
    221      13     3204       2156             16448   0 msdtc
    267      15     3112      15752       0.77  21240   1 notepad
    114       9     3872       2080       0.14   7804   1 nvapiw
    321      15     4868       6724              2800   0 NVDisplay.Container
    653      30    33144      18488              3728   1 NVDisplay.Container
    737      28    37980      15236              5232   0 OfficeClickToRun
    439      53    31360      18028              5632   0 OneApp.IGCC.WinService
    901      59    50912      32504      25.39  11996   1 OneDrive
    866      39   160512     164860       3.00  11028   1 powershell
    234      27    24344       5156              4904   0 PresentationFontCache
      0      12     7976      66328               144   0 Registry
    149       8     1548       1204              6256   0 RstMwService
    299      12     2612       3528              6240   0 RtkAudUService64
    230      10     1900       2532              7328   1 RtkAudUService64
    325      13     3960       4700       0.50  11724   1 RtkAudUService64
    356      20     6824      17648       1.92   1648   1 RuntimeBroker
    228      15     6344       6392       0.42   8028   1 RuntimeBroker
    327      19     7060      15880       2.55   8460   1 RuntimeBroker
    431      21    12044      20700       0.81   8760   1 RuntimeBroker
    873      34    17124      37148       6.42   9128   1 RuntimeBroker
    142       9     1904       7416       0.09   9348   1 RuntimeBroker
    323      16     4232      14052       5.08   9820   1 RuntimeBroker
    395      22    10592      24116      21.08  15768   1 RuntimeBroker
    109       7     1464       1448       0.06  16184   1 RuntimeBroker
    154      10     2028       9580             22248   0 scrcons
    127       8     1376       6408             23948   0 SearchFilterHost
    769      74    40420      37280              8672   0 SearchIndexer
    377      13     2448      12588             23920   0 SearchProtocolHost
   1847     115   155560     220020      23.20   8652   1 SearchUI
    384      15     4216       7108             11648   0 SecurityHealthService
    163      10     2012       2496       0.09  11616   1 SecurityHealthSystray
    854      12     6536       7124              1020   0 services
   1193      55    60520      38452             15652   0 ServiceShell
     49       4      700        784              6232   0 SessionService
    607      26    12744       4040      59.17   4484   1 SettingSyncHost
     89       7     3564       3988             11436   0 SgrmBroker
    872      34    22484      20936       7.33  13860   1 ShellExperienceHost
    611      19     8712      21140      27.33   4760   1 sihost
    313      17   107368      30708              6268   0 SmartByteNetworkService
     53       3     1148        276               624   0 smss
    433      21     5620       5160              4132   0 spoolsv
    723      33    35320      57248      12.89   8208   1 StartMenuExperienceHost
   1031      92    87436      48948     781.06  12360   1 steam
    248      15     6728       3928             12820   0 SteamService
    529      50   102036      44696      32.66   6604   1 steamwebhelper
    500      29    27756      18860       0.36   6968   1 steamwebhelper
    374      23    14652      18540       8.67   9652   1 steamwebhelper
   1363      33   226472      54600   2,090.53  12524   1 steamwebhelper
    769      35    23284      31240      14.02  12784   1 steamwebhelper
    254      17     7892       5696       0.30  12824   1 steamwebhelper
    554      62    89824      61668   1,432.98  14904   1 steamwebhelper
   1140      77   600776     111404             17084   0 SupportAssistAgent
     86       5      984        964               576   0 svchost
   1318      25    16064      25084              1056   0 svchost
   1617      21    11476      13792              1180   0 svchost
    305      10     2856       3608              1232   0 svchost
    250      10     2196       2124              1252   0 svchost
    187      11     2280       3564              1536   0 svchost
    151      31     7864       4520              1568   0 svchost
    175      12     2036       2168              1584   0 svchost
    161      10     1972       4192              1592   0 svchost
    270      19     2972       4556              1620   0 svchost
    392      16     3092       4844              1804   0 svchost
    258      14     2860       5112              1832   0 svchost
    224      13     2828       5232              1840   0 svchost
    406      18     6648       9216              1848   0 svchost
    155      10     2132       4472              1860   0 svchost
    450      15    17160      12628              1880   0 svchost
    318      12     3056       5504              2064   0 svchost
    169      13     2016       1604              2076   0 svchost
    175       7     1644       1788              2096   0 svchost
    440      10     3004       4928              2128   0 svchost
    247      11     2756       4076              2172   0 svchost
    183      12     2012       4088              2448   0 svchost
    388      16     5684       8128              2572   0 svchost
    265      12     2780       4432              2580   0 svchost
    297      14     4024       5172              2620   0 svchost
    148       8     1628       2348              2672   0 svchost
    124       8     1716       1384              2708   0 svchost
    210      11     2704       3768              2744   0 svchost
    184      12     2488       6664              2888   0 svchost
    213      13     9020      13504              2992   0 svchost
    232      16     3340       6136              3004   0 svchost
    263       7     1356       1376              3016   0 svchost
    221      12     2596       2212              3060   0 svchost
    869      15     3724       6724              3184   0 svchost
    168       9     1788       5720              3248   0 svchost
    186       9     1852       3484              3260   0 svchost
    433      14     4464       8420              3528   0 svchost
    488      17    12468      17064              3752   0 svchost
    136      12     1868       2580              3896   0 svchost
    570      25     7424      11360              3972   0 svchost
    252      15     2912       6668              4056   0 svchost
    538      24     5016      10016              4160   0 svchost
    294      16     7232       4812              4196   0 svchost
    427      33     8736       9224              4236   0 svchost
    188      11     2136       2396              4284   0 svchost
    231      17     2380       3276              4384   0 svchost
    184      10     1924       5448              4624   0 svchost
    784      77    68852      25276              4656   0 svchost
    555      21    11008      23936      18.25   4800   1 svchost
    165       9     1976       3116       3.33   4836   1 svchost
    115       7     1284       4792              4912   0 svchost
   1339      24    11496      27240      39.64   4952   1 svchost
    255      13     3476      10208              5112   0 svchost
    537      25    18292      19668              5132   0 svchost
    172       9     2004       2016              5180   0 svchost
    392      25    26048      19312              5272   0 svchost
    209      12     2464       4180              5740   0 svchost
    450      26     5936      12352              5948   0 svchost
    264      14     2776       2600              6048   0 svchost
    162      10     1876       3356              6056   0 svchost
    410     572    23248      17228              6128   0 svchost
    219      13     2628       2380              6248   0 svchost
    172      10     2208       2660              6312   0 svchost
    414      20     5160      13284              6400   0 svchost
    128       7     1336       1836              6408   0 svchost
    447      21     7560      21104       3.00   6860   1 svchost
    151       9     1796       3352              7000   0 svchost
    106       7     1328       1104              7052   0 svchost
    132       9     1668       1100              7152   0 svchost
    174      11     2276       3952              7348   0 svchost
    416      27     3640       5352              7484   0 svchost
    318      17     5044      10892              8008   0 svchost
    460      22     7832      12308              8472   0 svchost
    139       9     1748       2384              9596   0 svchost
    200      11     2108       7912              9680   0 svchost
    209      13     2764       4092              9740   0 svchost
    266      16     2936       4324              9996   0 svchost
    295      14     4492       6316             11372   0 svchost
    337      12     6580      10420             14320   0 svchost
    267      14     3672      10796             14932   0 svchost
    484      26     5672      11860       1.89  15632   1 svchost
    303      14     3460       4244             16196   0 svchost
    238      13     3132       6016             16860   0 svchost
    192      15     6800       4492             17224   0 svchost
    194      12     2504       8768             17332   0 svchost
    287      13     2820       5084             17740   0 svchost
    230      16     4164      14268             17988   0 svchost
    112       8     1812       5692             19256   0 svchost
   6005       0      196         20                 4   0 System
    871      42    27720         92       2.47   7024   1 SystemSettings
    316      32     7272       9648       4.23   5100   1 taskhostw
    321      18     5436      15608       0.17  10036   1 taskhostw
    247      14    18556       9180              6416   0 ThunderboltService
    119       9     1996       2352       0.13   8112   1 UserOOBEBroker
    246      18     3148      11916              3412   0 vds
    800      27    85580      15396      18.88  11772   1 WavesSvc64
    495      21     8360       9776              8496   0 WavesSysSvc64
    509      22    13084      21436       7.08    224   1 WindowsInternal.ComposableShell.Experiences.TextInput.InputApp
    159      11     1348        844               948   0 wininit
    276      13     3080       4732              1288   1 winlogon
   1024      70    53484       1268       1.13    960   1 WinStore.App
    102       6     1272       1720              2792   0 wlanext
    478      23    30568      37772              4312   0 WmiPrvSE
    182      13     4552       6048              8284   0 WmiPrvSE
    271      12     7068      12496             24496   0 WmiPrvSE
    316      14     5572      13232             24540   0 WmiPrvSE
    621      15     9884       5128              1104   0 WUDFHost
    384      18    10652      12352              3048   0 WUDFHost
    716      46    27348       1112       1.45  20468   1 YourPhone
```
* choisissez 5 services système et expliquer leur utilité

**lsass** 

lsass vérifie les utilisateurs qui s'enregistrent à un ordinateur Windows, traite les modifications de mot de passe et crée des jetons d'accès qui encapsulent les informations essentielles sur la sécurité.

**Taskhostw**

Taskhostw est un processus hôte pour Tâches Windows , c'est un fichier servant d’hôte aux processus basés sur les DLL. Dans le gestionnaire de tâches, ces processus sont affichés avec le nom Processus hôte pour les tâches Windows.

**wininit**

Wininit gere  l'exécution du processus d'initialisation  Windows 

**smss**

smss effectue les opérations suivantes :
    Crée des variables d'environnement
    Charge la clé BootExecute ce qui permet de lancer un chkdsk au démarrage de Windows
    Démarre les modes noyau et utilisateur du sous-système Win32. Ce sous-système comprend
        win32k.sys (mode noyau),
        winsrv.dll (mode utilisateur)
        csrss.exe (mode utilisateur).
    Crée des mappages de périphériques DOS(COMX, LPT, CON, etc
    Crée des fichiers de pagination de mémoire virtuelle
    Lance winlogon.exe, le gestionnaire de connexion Windows

**svchost** 

Au démarrage, svchost vérifie le registre pour des services chargeant un fichier .dll externe et les démarrent.

* déterminer les processus lancés par l'utilisateur qui est full admin sur la machine

```
C:\WINDOWS\system32> Get-Process -IncludeUserName

Handles      WS(K)   CPU(s)     Id UserName               ProcessName
-------      -----   ------     -- --------               -----------
    410      23592     3.39  14868 DESKTOP-HO10B5L\rukab  ApplicationFrameHost
    520      21460 2,588.28  14424 NT AUTHORITY\LOCAL ... audiodg
   4663     180552 3,488.00   4792                        avp
   1222       9092    34.52   8116 DESKTOP-HO10B5L\rukab  avpui
     71       3592     0.00   1712 DESKTOP-HO10B5L\rukab  cmd
    320      36388     1.81   3124 DESKTOP-HO10B5L\rukab  Code
    644      18212     3.03  10008 DESKTOP-HO10B5L\rukab  Code
    727      42120     4.20  11300 DESKTOP-HO10B5L\rukab  Code
    460      53016    13.30  18712 DESKTOP-HO10B5L\rukab  Code
    252      10540     0.05  19792 DESKTOP-HO10B5L\rukab  Code
    441      22540     0.50  20472 DESKTOP-HO10B5L\rukab  Code
    399      36412     1.53  20620 DESKTOP-HO10B5L\rukab  Code
    163       3308     0.98   5732 DESKTOP-HO10B5L\rukab  CompPkgSrv
    118       1044     0.02   3340 NT AUTHORITY\SYSTEM    conhost
    265      18936     0.28  20376 DESKTOP-HO10B5L\rukab  conhost
    264      16868     2.06  20720 DESKTOP-HO10B5L\rukab  conhost
    878       2256     4.08    840                        csrss
    779       3164   254.89    964                        csrss
    491       9828    25.80   5276 DESKTOP-HO10B5L\rukab  ctfmon
    361       7016     3.45   2984 NT AUTHORITY\LOCAL ... dasHost
    156       1800     0.09  16028 NT AUTHORITY\SYSTEM    DDVCollectorSvcApi
    357      14268    23.61   3376 NT AUTHORITY\SYSTEM    DDVDataCollector
    266       5544     2.08  12928 NT AUTHORITY\SYSTEM    DDVRulesProcessor
    894      46124    25.92   9384 NT AUTHORITY\SYSTEM    Dell.D3.WinSvc
    982      38692    31.25   3032 NT AUTHORITY\SYSTEM    DellSupportAssistRemedationService
    931      85468   162.05  11276 DESKTOP-HO10B5L\rukab  Discord
    473      22604    30.78  11568 DESKTOP-HO10B5L\rukab  Discord
    935      48516   268.27  12204 DESKTOP-HO10B5L\rukab  Discord
    249       7668     0.25  14620 DESKTOP-HO10B5L\rukab  Discord
    939     185276 4,478.67  14660 DESKTOP-HO10B5L\rukab  Discord
    331      13332     1.59  14780 DESKTOP-HO10B5L\rukab  Discord
    241       8556     0.38   8392 DESKTOP-HO10B5L\rukab  dllhost
    198       4608     0.20  11484 NT AUTHORITY\SYSTEM    dllhost
    249       4844     0.81  17352 NT AUTHORITY\SYSTEM    dllhost
    719     108136    59.70  16288 NT AUTHORITY\SYSTEM    DSAPI
   1364      56564 3,570.58   1436 Window Manager\DWM-1   dwm
    107       1284     0.02    816 NT AUTHORITY\SYSTEM    esif_uf
   3136     145276   505.05   5648 DESKTOP-HO10B5L\rukab  explorer
    238      17024 1,973.70   5568 DESKTOP-HO10B5L\rukab  firefox
   1412     418120 4,229.94   8256 DESKTOP-HO10B5L\rukab  firefox
    874      71084   159.30  12712 DESKTOP-HO10B5L\rukab  firefox
   2515     375908 6,077.09  12944 DESKTOP-HO10B5L\rukab  firefox
    944     189060 1,675.02  13156 DESKTOP-HO10B5L\rukab  firefox
   1856     348232 ...26.53  13200 DESKTOP-HO10B5L\rukab  firefox
    919     200156   942.39  13320 DESKTOP-HO10B5L\rukab  firefox
    729     220508   283.27  13540 DESKTOP-HO10B5L\rukab  firefox
    937     189472 2,699.33  13736 DESKTOP-HO10B5L\rukab  firefox
    922     211652   861.52  15624 DESKTOP-HO10B5L\rukab  firefox
    950     396144 2,042.36  17060 DESKTOP-HO10B5L\rukab  firefox
    904     146428 2,199.89  17576 DESKTOP-HO10B5L\rukab  firefox
    909     187780   826.13  17884 DESKTOP-HO10B5L\rukab  firefox
     31       1456     0.08   1048 Font Driver Host\UM... fontdrvhost
     31       4116     0.72   1340 Font Driver Host\UM... fontdrvhost
    181       1120     0.05   9664 NT AUTHORITY\SYSTEM    GoogleCrashHandler
    160        152     0.02   9708 NT AUTHORITY\SYSTEM    GoogleCrashHandler64
      0          8               0                        Idle
    383      12596     0.64  12320 DESKTOP-HO10B5L\rukab  IGCC
    537      33328     4.73  11892 DESKTOP-HO10B5L\rukab  IGCCTray
    179       1984     0.08   2688 NT AUTHORITY\SYSTEM    igfxCUIService
   1192      14488     8.63   5904 DESKTOP-HO10B5L\rukab  igfxEM
    376      13580     0.98   5672 NT AUTHORITY\SYSTEM    IntelAudioService
    165       3640     0.44   6120 NT AUTHORITY\SYSTEM    IntelCpHDCPSvc
    137       2988     0.30   6812 NT AUTHORITY\SYSTEM    IntelCpHeciSvc
    125        800     0.02   6660 NT AUTHORITY\SYSTEM    jhi_service
    159       2368     0.05  12964 DESKTOP-HO10B5L\rukab  jusched
    799       5068    26.55   9636 NT AUTHORITY\SYSTEM    ksde
    312       2664     1.39  17616 DESKTOP-HO10B5L\rukab  ksdeui
    261       3848     0.89   5944 NT AUTHORITY\SYSTEM    LMS
    553      30764     5.58  16652 DESKTOP-HO10B5L\rukab  LockApp
   1880      17440    26.66    108 NT AUTHORITY\SYSTEM    lsass
      0     270996   400.30   3192                        Memory Compression
    976       1140     1.19  15756 DESKTOP-HO10B5L\rukab  Microsoft.Photos
    221       2052     0.05  16448 NT AUTHORITY\NETWOR... msdtc
    267      15684     0.77  21240 DESKTOP-HO10B5L\rukab  notepad
    114       2064     0.14   7804 DESKTOP-HO10B5L\rukab  nvapiw
    335       6904     2.56   2800 NT AUTHORITY\SYSTEM    NVDisplay.Container
    668      18996    26.69   3728 NT AUTHORITY\SYSTEM    NVDisplay.Container
    743      31064    11.02   5232 NT AUTHORITY\SYSTEM    OfficeClickToRun
    564      17896     0.81   5632 NT AUTHORITY\SYSTEM    OneApp.IGCC.WinService
    917      42076    25.72  11996 DESKTOP-HO10B5L\rukab  OneDrive
    742      79068     0.69  15376 DESKTOP-HO10B5L\rukab  powershell
    234       5024     0.30   4904 NT AUTHORITY\LOCAL ... PresentationFontCache
      0      65820     2.61    144                        Registry
    149       1192     0.17   6256 NT AUTHORITY\SYSTEM    RstMwService
    299       3516     0.33   6240 NT AUTHORITY\SYSTEM    RtkAudUService64
    230       2540     0.11   7328 NT AUTHORITY\SYSTEM    RtkAudUService64
    325       4668     0.50  11724 DESKTOP-HO10B5L\rukab  RtkAudUService64
    381      19616     2.02   1648 DESKTOP-HO10B5L\rukab  RuntimeBroker
    228       6384     0.44   8028 DESKTOP-HO10B5L\rukab  RuntimeBroker
    327      15764     2.55   8460 DESKTOP-HO10B5L\rukab  RuntimeBroker
    431      20552     0.84   8760 DESKTOP-HO10B5L\rukab  RuntimeBroker
    896      37900     6.59   9128 DESKTOP-HO10B5L\rukab  RuntimeBroker
    142       7352     0.09   9348 DESKTOP-HO10B5L\rukab  RuntimeBroker
    338      18400     5.11   9820 DESKTOP-HO10B5L\rukab  RuntimeBroker
    393      23996    21.09  15768 DESKTOP-HO10B5L\rukab  RuntimeBroker
    109       1440     0.06  16184 DESKTOP-HO10B5L\rukab  RuntimeBroker
    127       6460     0.05  23628 NT AUTHORITY\SYSTEM    SearchFilterHost
    775      45356    44.63   8672 NT AUTHORITY\SYSTEM    SearchIndexer
    377      12748     0.02  22576 NT AUTHORITY\SYSTEM    SearchProtocolHost
   1994     231268    25.11   8652 DESKTOP-HO10B5L\rukab  SearchUI
    384       7004     1.63  11648                        SecurityHealthService
    163       2480     0.09  11616 DESKTOP-HO10B5L\rukab  SecurityHealthSystray
    861       7088 2,340.45   1020                        services
   1647      40444    11.80  15652 NT AUTHORITY\SYSTEM    ServiceShell
     49        780     0.00   6232 NT AUTHORITY\SYSTEM    SessionService
    607       4528    59.23   4484 DESKTOP-HO10B5L\rukab  SettingSyncHost
     89       3676     1.53  11436                        SgrmBroker
    864      50888     7.86  13860 DESKTOP-HO10B5L\rukab  ShellExperienceHost
    664      22220    27.77   4760 DESKTOP-HO10B5L\rukab  sihost
    315      34648   247.80   6268 NT AUTHORITY\SYSTEM    SmartByteNetworkService
    402      22808     0.05  21088 DESKTOP-HO10B5L\rukab  smartscreen
     53        276     0.16    624                        smss
    433       5188     1.20   4132 NT AUTHORITY\SYSTEM    spoolsv
    914      60268    13.09   8208 DESKTOP-HO10B5L\rukab  StartMenuExperienceHost
   1120      51136   797.05  12360 DESKTOP-HO10B5L\rukab  steam
    248       3892     1.48  12820 NT AUTHORITY\SYSTEM    SteamService
    529      47084    32.69   6604 DESKTOP-HO10B5L\rukab  steamwebhelper
    500      19844     0.36   6968 DESKTOP-HO10B5L\rukab  steamwebhelper
    374      18660     8.67   9652 DESKTOP-HO10B5L\rukab  steamwebhelper
   1365      62496 2,141.81  12524 DESKTOP-HO10B5L\rukab  steamwebhelper
    769      31908    14.02  12784 DESKTOP-HO10B5L\rukab  steamwebhelper
    254       5688     0.30  12824 DESKTOP-HO10B5L\rukab  steamwebhelper
    554      69144 1,464.83  14904 DESKTOP-HO10B5L\rukab  steamwebhelper
   1149     110956    45.78  17084 NT AUTHORITY\SYSTEM    SupportAssistAgent
     86        956     0.03    576 NT AUTHORITY\SYSTEM    svchost
   1349      25536    86.39   1056 NT AUTHORITY\SYSTEM    svchost
   1624      14136   162.72   1180 NT AUTHORITY\NETWOR... svchost
    307       3628     5.58   1232 NT AUTHORITY\SYSTEM    svchost
    250       2072     0.05   1252 NT AUTHORITY\SYSTEM    svchost
    187       2372     0.89   1536 NT AUTHORITY\SYSTEM    svchost
    159       6176    14.95   1568 NT AUTHORITY\LOCAL ... svchost
    173       2140     0.39   1584 NT AUTHORITY\LOCAL ... svchost
    161       4140     0.73   1592 NT AUTHORITY\LOCAL ... svchost
    269       4596     0.55   1620 NT AUTHORITY\LOCAL ... svchost
    392       4960     5.55   1804 NT AUTHORITY\LOCAL ... svchost
    256       5120     2.09   1832 NT AUTHORITY\SYSTEM    svchost
    230       5392     0.30   1840 NT AUTHORITY\SYSTEM    svchost
    403       9168    11.80   1848 NT AUTHORITY\SYSTEM    svchost
    163       4572     0.75   1860 NT AUTHORITY\LOCAL ... svchost
    451      12872   180.55   1880 NT AUTHORITY\LOCAL ... svchost
    324       5548     5.95   2064 NT AUTHORITY\SYSTEM    svchost
    169       1596     0.03   2076 NT AUTHORITY\SYSTEM    svchost
    175       1800     0.34   2096 NT AUTHORITY\LOCAL ... svchost
    440       4972     2.20   2128 NT AUTHORITY\LOCAL ... svchost
    240       4080    48.13   2172 NT AUTHORITY\LOCAL ... svchost
    183       4036     1.27   2448 NT AUTHORITY\SYSTEM    svchost
    388       8128    35.77   2572 NT AUTHORITY\NETWOR... svchost
    257       4372    56.33   2580 NT AUTHORITY\LOCAL ... svchost
    298       5296 ...12.92   2620 NT AUTHORITY\NETWOR... svchost
    148       2320     0.27   2672 NT AUTHORITY\SYSTEM    svchost
    124       1376     0.05   2708 NT AUTHORITY\LOCAL ... svchost
    208       3856     0.77   2744 NT AUTHORITY\LOCAL ... svchost
    191       6680     9.39   2888 NT AUTHORITY\SYSTEM    svchost
    230      14008    57.52   2992 NT AUTHORITY\SYSTEM    svchost
    231       6284   340.53   3004 NT AUTHORITY\SYSTEM    svchost
    265       1384     0.39   3016 NT AUTHORITY\SYSTEM    svchost
    221       2216     0.34   3060 NT AUTHORITY\LOCAL ... svchost
    893       6804   142.52   3184 NT AUTHORITY\LOCAL ... svchost
    170       5500     6.63   3248 NT AUTHORITY\LOCAL ... svchost
    186       3456     1.08   3260 NT AUTHORITY\SYSTEM    svchost
    448       8804    23.02   3528 NT AUTHORITY\LOCAL ... svchost
    563      16884    56.50   3752 NT AUTHORITY\SYSTEM    svchost
    136       2604     3.55   3896 NT AUTHORITY\LOCAL ... svchost
    572      11340    34.72   3972 NT AUTHORITY\SYSTEM    svchost
    252       6616     0.45   4056 NT AUTHORITY\SYSTEM    svchost
    538      11080    15.13   4160 NT AUTHORITY\SYSTEM    svchost
    294       4768     0.34   4196 NT AUTHORITY\SYSTEM    svchost
    424       9260    25.42   4236 NT AUTHORITY\LOCAL ... svchost
    186       2416     2.70   4284 NT AUTHORITY\NETWOR... svchost
    231       3340     8.14   4384 NT AUTHORITY\LOCAL ... svchost
    192       5516     0.70   4624 NT AUTHORITY\SYSTEM    svchost
    775      31528   371.45   4656 NT AUTHORITY\SYSTEM    svchost
    550      23968    19.80   4800 DESKTOP-HO10B5L\rukab  svchost
    165       3076     3.33   4836 DESKTOP-HO10B5L\rukab  svchost
    115       4740     0.02   4912 NT AUTHORITY\LOCAL ... svchost
   1377      28736    40.00   4952 DESKTOP-HO10B5L\rukab  svchost
    282      15048     9.86   5112 NT AUTHORITY\SYSTEM    svchost
    551      20396    10.44   5132 NT AUTHORITY\SYSTEM    svchost
    172       1984     0.06   5180 NT AUTHORITY\SYSTEM    svchost
    385      29956   244.92   5272 NT AUTHORITY\LOCAL ... svchost
    207       4200     0.69   5740 NT AUTHORITY\SYSTEM    svchost
    434      12456     5.63   5948 NT AUTHORITY\LOCAL ... svchost
    264       2964    19.67   6048 NT AUTHORITY\SYSTEM    svchost
    162       3312    10.59   6056 NT AUTHORITY\NETWOR... svchost
    327      17100     9.50   6128 NT AUTHORITY\NETWOR... svchost
    219       2388     0.17   6248 NT AUTHORITY\NETWOR... svchost
    172       2620     4.72   6312 NT AUTHORITY\LOCAL ... svchost
    414      13312     3.28   6400 NT AUTHORITY\SYSTEM    svchost
    128       1780     0.02   6408 NT AUTHORITY\SYSTEM    svchost
    456      21184     4.03   6860 DESKTOP-HO10B5L\rukab  svchost
    151       3276     0.23   7000 NT AUTHORITY\SYSTEM    svchost
    106       1088     0.33   7052 NT AUTHORITY\LOCAL ... svchost
    132       1092     0.08   7152 NT AUTHORITY\LOCAL ... svchost
    174       3892     2.89   7348 NT AUTHORITY\LOCAL ... svchost
    416       5304     0.73   7484 NT AUTHORITY\SYSTEM    svchost
    318      10804    34.30   8008 NT AUTHORITY\SYSTEM    svchost
    484      12416    48.53   8472                        svchost
    139       2400     0.23   9596 NT AUTHORITY\LOCAL ... svchost
    200       7840     0.05   9680 NT AUTHORITY\LOCAL ... svchost
    209       4056     0.47   9740 NT AUTHORITY\SYSTEM    svchost
    266       4308     6.25   9996 NT AUTHORITY\LOCAL ... svchost
    295       6248     0.55  11372 NT AUTHORITY\SYSTEM    svchost
    337      10328     4.34  14320 NT AUTHORITY\SYSTEM    svchost
    275      10872     1.83  14932 NT AUTHORITY\LOCAL ... svchost
    483      11772     1.89  15632 DESKTOP-HO10B5L\rukab  svchost
    301       4248     1.30  16196                        svchost
    238       6076    22.30  16860 NT AUTHORITY\SYSTEM    svchost
    192       4504     0.22  17224 NT AUTHORITY\SYSTEM    svchost
    194       8708     0.03  17332                        svchost
    287       5064     0.83  17740 NT AUTHORITY\SYSTEM    svchost
    111       5624     0.02  19256 NT AUTHORITY\SYSTEM    svchost
    249      15064     0.11  19384 NT AUTHORITY\NETWOR... svchost
    118       7724     0.05  22960                        svchost
    396      17284     0.06  24164 NT AUTHORITY\SYSTEM    svchost
   6194         20 7,213.16      4                        System
    871         20     2.47   7024 DESKTOP-HO10B5L\rukab  SystemSettings
    320      11436     4.30   5100 DESKTOP-HO10B5L\rukab  taskhostw
    316      15632     0.19  10036 DESKTOP-HO10B5L\rukab  taskhostw
    247       9092     0.19   6416 NT AUTHORITY\SYSTEM    ThunderboltService
    119       2260     0.13   8112 DESKTOP-HO10B5L\rukab  UserOOBEBroker
    244      11720     0.08   3412 NT AUTHORITY\SYSTEM    vds
    818      16132    18.97  11772 DESKTOP-HO10B5L\rukab  WavesSvc64
    495       9692    23.38   8496 NT AUTHORITY\SYSTEM    WavesSysSvc64
    509      21464     8.47    224 DESKTOP-HO10B5L\rukab  WindowsInternal.ComposableShell.Experiences.TextInput.Inpu...
    159        832     0.05    948                        wininit
    278       4740     0.91   1288 NT AUTHORITY\SYSTEM    winlogon
   1024        820     1.13    960 DESKTOP-HO10B5L\rukab  WinStore.App
    102       1700     0.19   2792 NT AUTHORITY\SYSTEM    wlanext
    472      37968    55.13   4312 NT AUTHORITY\SYSTEM    WmiPrvSE
    182       6004     2.58   8284 NT AUTHORITY\NETWOR... WmiPrvSE
    238      14448     0.17  20280 NT AUTHORITY\NETWOR... WmiPrvSE
    621       5056     6.59   1104 NT AUTHORITY\LOCAL ... WUDFHost
    384      12268    36.80   3048 NT AUTHORITY\LOCAL ... WUDFHost
    716        804     1.45  20468 DESKTOP-HO10B5L\rukab  YourPhone
```
## Network

### Afficher la liste des cartes réseau de votre machine
```
PS C:\Users\rukab> ipconfig

Configuration IP de Windows


Carte Ethernet VirtualBox Host-Only Network :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::5096:5111:e180:6643%19
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.56.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :

Carte réseau sans fil Connexion au réseau local* 1 :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :

Carte réseau sans fil Connexion au réseau local* 2 :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :

Carte Ethernet Ethernet 2 :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : home
   Adresse IPv6. . . . . . . . . . . . . .: 2a01:cb19:3b:b700:aca5:d93:29fe:9e2c
   Adresse IPv6 temporaire . . . . . . . .: 2a01:cb19:3b:b700:d09c:2546:d0d8:cf05
   Adresse IPv6 de liaison locale. . . . .: fe80::aca5:d93:29fe:9e2c%11
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.34
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . : fe80::a63e:51ff:fe68:2a46%11
                                       192.168.1.1

Carte Ethernet Connexion réseau Bluetooth :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :

Carte Tunnel Teredo Tunneling Pseudo-Interface :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6. . . . . . . . . . . . . .: 2001:0:1428:8f18:2433:1b0b:a92a:b8f6
   Adresse IPv6 de liaison locale. . . . .: fe80::2433:1b0b:a92a:b8f6%14
   Passerelle par défaut. . . . . . . . . :
```
* expliquer la fonction de chacune d'entre elles

**[01]: VirtualBox** : cet carte permet au machine virtuel editer sur VirtualBox de ce connecter a un reseau
**[02]: Carte réseau sans fil Connexion au réseau local** : cet carte permet de se connecter en sans fil au réseau local
**[03]: Carte Ethernet Ethernet 2** : cet carte sert a se connecter en ethernet au réseau
**[04]: Carte réseau sans fil Wi-Fi** : cet carte est utilisé pour se connecter au réseau WI-FI
**[05]: Carte Ethernet Connexion réseau Bluetooth** : cet carte est utilisé pour ce connecter a un réseau Bluetooth
**[06]: Carte Tunnel Teredo Tunneling Pseudo-Interface** : Teredo est une technologie permettant d'encapsuler des paquets IPv6 dans des paquets IPv4 lorsque les périphériques réseau ne supportent pas la norme IPv6

### Lister tous les ports TCP et UDP en utilisation

* déterminer quel programme tourne derrière chacun des ports

```
PS C:\Windows\system32> netstat -a -b

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            DESKTOP-HO10B5L:0      LISTENING
  RpcSs
 [svchost.exe]
  TCP    0.0.0.0:445            DESKTOP-HO10B5L:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:808            DESKTOP-HO10B5L:0      LISTENING
 [OneApp.IGCC.WinService.exe]
  TCP    0.0.0.0:5040           DESKTOP-HO10B5L:0      LISTENING
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:5357           DESKTOP-HO10B5L:0      
 LISTENING
 [steam.exe]
  TCP    0.0.0.0:49664          DESKTOP-HO10B5L:0      LISTENING

  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          DESKTOP-HO10B5L:0      LISTENING
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49668          DESKTOP-HO10B5L:0      LISTENING
 [spoolsv.exe]
  TCP    0.0.0.0:49670          DESKTOP-HO10B5L:0      
 [firefox.exe]
  TCP    127.0.0.1:49680        DESKTOP-HO10B5L:49679  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:49681        DESKTOP-HO10B5L:49682  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:49682        DESKTOP-HO10B5L:49681  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:49683        DESKTOP-HO10B5L:49684  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:49684        DESKTOP-HO10B5L:49683  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:49685        DESKTOP-HO10B5L:49686  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:49686        DESKTOP-HO10B5L:49685  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:49687        DESKTOP-HO10B5L:49688  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:49688        DESKTOP-HO10B5L:49687  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:49843        DESKTOP-HO10B5L:0      LISTENING
 [DSAPI.exe]
  TCP    127.0.0.1:49865        DESKTOP-HO10B5L:49866  ESTABLISHED
 [ksde.exe]
  TCP    127.0.0.1:49866        DESKTOP-HO10B5L:49865  ESTABLISHED
 [ksde.exe]
  TCP    127.0.0.1:50059        DESKTOP-HO10B5L:50060  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:50060        DESKTOP-HO10B5L:50059  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:50377        DESKTOP-HO10B5L:50378  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50378        DESKTOP-HO10B5L:50377  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50384        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50694        DESKTOP-HO10B5L:50695  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50695        DESKTOP-HO10B5L:50694  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50758        DESKTOP-HO10B5L:50759  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50759        DESKTOP-HO10B5L:50758  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50760        DESKTOP-HO10B5L:50761  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50761        DESKTOP-HO10B5L:50760  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50764        DESKTOP-HO10B5L:50765  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50765        DESKTOP-HO10B5L:50764  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50858        DESKTOP-HO10B5L:50859  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50859        DESKTOP-HO10B5L:50858  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50918        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:50923        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:51243        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:51249        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:51275        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:51279        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:51304        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:51309        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:51323        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:51326        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:51618        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:51623        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:51651        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:51659        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:52336        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:52347        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:52622        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:52636        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:52679        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:52686        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:52855        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:52860        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:52864        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:52872        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:52904        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:52914        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:53220        DESKTOP-HO10B5L:53221  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:53221        DESKTOP-HO10B5L:53220  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:54862        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:54877        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:55002        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:55112        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:55122        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:55176        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:55184        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:55304        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:55309        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:55802        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56054        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56065        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56105        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56115        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56173        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56177        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56321        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56328        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56537        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56538        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56541        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56542        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56841        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56957        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56958        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:56963        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57020        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57031        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57042        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57308        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57310        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57610        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57620        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57621        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57623        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57643        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57862        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57864        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57894        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57899        DESKTOP-HO10B5L:57898  TIME_WAIT
  TCP    127.0.0.1:57958        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57959        DESKTOP-HO10B5L:49675  ESTABLISHED
 [firefox.exe]
  TCP    127.0.0.1:57974        DESKTOP-HO10B5L:57975  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:57975        DESKTOP-HO10B5L:57974  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.34:139       DESKTOP-HO10B5L:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.34:49422     40.67.254.36:https     ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    192.168.1.34:57144     ec2-34-223-130-205:https  ESTABLISHED
 [firefox.exe]
  TCP    192.168.1.34:57154     104.19.236.56:http     CLOSE_WAIT
 [SearchUI.exe]
  TCP    192.168.1.34:57177     185.25.182.77:https    ESTABLISHED
 [steam.exe]
  TCP    192.168.1.34:57364     104.244.42.2:https     ESTABLISHED
 [firefox.exe]
  TCP    192.168.1.34:57432     162.159.135.234:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.1.34:57849     162.159.135.233:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.1.34:57888     par10s39-in-f4:https   ESTABLISHED
 [firefox.exe]
  TCP    192.168.1.34:57889     81.19.104.91:https     TIME_WAIT
  TCP    192.168.1.34:57892     81.19.104.91:https     TIME_WAIT


```
* expliquer la fonction de chacun de ces programmes

**[firefox.exe]** : firefox est un navigateur internet
**[steam.exe]** : steam est une platforme de jeux vidéo
**[svchost.exe]** :  svchost signifie « Service Host Process » et sert d'hôte pour les fonctionnalités de bibliothèques de liens dynamiques
**[spoolsv.exe]** : spoolsv.exe est le principal service de file d'attente de l'imprimante sous Windows 2000 et les versions suivantes – il s'occupe de gérer tout le travail d'impressions sur l'ordinateur. c'est aussi un processus sécuritaire du système de Microsoft Windows, appelé "Spooler SubSystem App".
**[Discord.exe]** : Discord est un outil de communication
**[SearchUI.exe]** : SearchUI.exe active l'interface utilisateur de recherche de l'assistant de recherche Microsoft Cortana
**[ksde.exe]** : ksde fait partie de Kaspersky Anti-Virus
**[DSAPI.exe]** : Le processus appelé Dell Hardware Support appartient au logiciel PC-Doctor pour Windows de PC-Doctor 
**[SupportAssistAgent.exe]** :  SupportAssistAgent.exe est un composant logiciel de Dell SupportAssistAgent de Dell
**[OneApp.IGCC.WinService.exe]** : Ce sont les pilotes pour le centre de commande graphique Intel


# II SCRIPTING

# III. Gestion de softs

* interet d'un gestionaire de paquets

un gestionaire de paquet permet de centraliser les package dans un meme depot (repository) ce qui simplifie leur recherche et leur installation mais aussi le niveau de securité les repository étant souvent geré par les créateur de l'os il est plus prudent de telecharger ces soft avec un gestionaire de paquet qui implique un seul intermediare que sur un site ou les paquet peuvent avoir était modifié 

* liste des packet instalés

```
PS C:\Windows\system32> choco list -l
```

```
[...]
Chocolatey v0.10.15
chocolatey 0.10.15
1 packages installed.
```

* origine des paquets
```
PS C:\Windows\system32> choco source list
```
l'adresse du repo est- https://chocolatey.org/api/v2/ 

# IV. Machine Virtuelle

* connection ssh

afin de demaré une connection ssh j'utilise cette comande dans powershell

```
PS C:\Users\rukab> ssh root@192.168.120.50  
```

une fois la commande executé mon terminal me renvoi ceci

```
root@192.168.120.50's password:
Last login: Mon Nov  9 16:35:05 2020
[root@tpworkstation ~]#  
```

## Partage de fichiers