﻿#Script for exececute actions set in argument 
#Auteur : Diologent Valentin
#Date : 06/11/2020

if ($args[0] -match "lock") {
    Start-Sleep -Seconds $args[1]                #if for lock the screen after the time set in argument
    rundll32.exe user32.dll, LockWorkStation
}
elseif ($args -match "shutdown") {
    shutdown /s /t $args[1]                      # eslseif for shutdown the computer after time set in argument
}
else {
    echo "wrong arguments"                       #else for say if the argument are good or not
}